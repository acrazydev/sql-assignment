## *_ 1. list of consultants having more than one submission - _*

```SELECT *  FROM consultant c WHERE c.id in (
    SELECT consult_id from submission GROUP BY consult_id
    HAVING COUNT(consult_id) > 1
);```

## *2. list all the interviews of consultants*

```SELECT c.c_name, i.start_time, i.end_time, i.result, i.round, i.remarks from submission s LEFT JOIN consultant c on s.consult_id = c.id LEFT JOIN interview i on s.sub_id = i.sub_id;```


## *3. list all PO of marketers*

```select e.e_name, e.email, p.p_id as purchase_id from employee e
    LEFT JOIN roles_employee re on e.e_id = re.emp_id
    LEFT JOIN roles r on re.role_id = r.role_id
	inner join submission s on e.e_id = s.emp_id
	inner join purchase_order p on s.sub_id = p.sub_id
	WHERE r.role_name = 'Marketer';```

## *4. unique vendor company name for which client location is the same*

```SELECT DISTINCT(v.com_name) FROM submission s
    LEFT JOIN vendor v on s.vendor_id = v.ven_id
    LEFT JOIN client c on s.client_id = c.client_id
    WHERE c.city = s.city;```


## *5. count of consultants which submitted in the same city*

```SELECT count(*) from submission s
    INNER JOIN consultant c on s.city = c.city
    GROUP BY c.city;```

## *6. name of consultant and client who have been submitted on the same vendor*

```SELECT c.c_name, cl.client_name from submission s
    LEFT JOIN consultant c on s.consult_id = c.id
    LEFT JOIN client cl on s.client_id = cl.client_id;```